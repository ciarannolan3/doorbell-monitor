import os
import time
import datetime
import json
import http.client, urllib

def follow(thefile):
    thefile.seek(0, 2)
    while True:
        now = datetime.datetime.now()
        # if now.hour==0:
        # 	os.system('> messages.json') # clear the file
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            continue
        yield line


def checkTransmission(radioTransmission):
	if radioTransmission["model"] == 'Smoke-GS558' and radioTransmission["id"] == 6570:
		print("Doorbell pressed")
		conn = http.client.HTTPSConnection("api.pushover.net:443")
		conn.request("POST", "/1/messages.json",
			urllib.parse.urlencode({
				"token": "aq6wtcibseu5fkkfpq9mie3cqjieo9",
				"user": "uuyzjjvxr858pjr9iyxem6drq629s9",
				"message": "Doorbell Pressed!",
				}), 
			{ "Content-type": "application/x-www-form-urlencoded" })
		conn.getresponse()


if __name__ == '__main__':
    logfile = open("messages.json", "r")
    loglines = follow(logfile)
    for line in loglines:
        radioTransmission = json.loads(line)
        checkTransmission(radioTransmission)
        print(radioTransmission["id"])


